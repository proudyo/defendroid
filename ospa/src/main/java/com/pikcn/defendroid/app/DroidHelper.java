package com.pikcn.defendroid.app;


import android.app.KeyguardManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.icu.text.SimpleDateFormat;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Locale;

public final class DroidHelper {

    public static final int FLAGS = PackageManager.GET_ACTIVITIES |
            PackageManager.GET_CONFIGURATIONS |
            PackageManager.GET_GIDS |
            PackageManager.GET_INSTRUMENTATION |
            /*PackageManager.GET_INTENT_FILTERS |*/
            PackageManager.GET_META_DATA |
            PackageManager.GET_PERMISSIONS |
            PackageManager.GET_PROVIDERS |
            PackageManager.GET_RECEIVERS |
            PackageManager.GET_SERVICES |
            PackageManager.GET_SHARED_LIBRARY_FILES |
            PackageManager.GET_SIGNING_CERTIFICATES |
            PackageManager.GET_URI_PERMISSION_PATTERNS;

    private DroidHelper() { throw new UnsupportedOperationException(); }
    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.1 (L1) Ensure device firmware is up to date
     * Ensure that the device is kept up to date with security patch levels
     * Firmware updates often include critical security fixes that reduce the probability of an attacker remotely exploiting the device. The device should be on the latest security patch level as applicable
     *
     * @return true if the security patch date is not older than 2 months
     */
    public static boolean isSecurityPatched() {
        try {
            long twoMonths = 1000L * 60L * 60L * 24L * 60L;
            long patchDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(android.os.Build.VERSION.SECURITY_PATCH).getTime();
            long now = System.currentTimeMillis();
            return now - patchDate < twoMonths;
        } catch (ParseException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot parse " + android.os.Build.VERSION.SECURITY_PATCH, e);
        }
        return false;
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.2 (L1) Ensure 'Screen Lock' is set to 'Enabled'
     * Enabling Screen Lock requires user authentication before interacting with the device. As a result, Screen Lock strengthens application and data protection and improves device security
     *
     * @param context Application Context
     * @return true if PIN, pattern or password is set on the device
     */
    public static boolean isDeviceSecured(final Context context) {
        final KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        return keyguardManager.isDeviceSecure();
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.3 (L1) Ensure 'Make pattern visible' is set to 'Disabled' (if using a pattern as device lock mechanism)
     * Keeping device unlock patterns visible during device unlock can reveal the pattern and is vulnerable to shoulder surfing attacks. Therefore, is it not a best practice to make the device pattern visible.
     *
     * @param context Application Context
     * @return true if using a pattern as device lock mechanism and pattern is visible
     */
    public static boolean isPatternVisible(final Context context) {
        try {
            int visible = Settings.Secure.getInt(context.getContentResolver(), "lock_pattern_visible_pattern", -1);
            return visible == 1;
        } catch (final SecurityException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot get Settings.Secure lock_pattern_visible_pattern", e);
            return false;
        }
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.4 (L1) Ensure 'Make pattern visible' is set to 'Disabled' (if using a pattern as device lock mechanism)
     * Keeping device unlock patterns visible during device unlock can reveal the pattern and is vulnerable to shoulder surfing attacks. Therefore, is it not a best practice to make the device pattern visible.
     *
     * @param context Application Context
     * @return true if using a pattern as device lock mechanism and pattern is visible
     */
    public static boolean isLockScreenTimeoutZero(final Context context) {
        try {
            final int timeout = Settings.Secure.getInt(context.getContentResolver(), "lock_screen_lock_after_timeout", -1);
            return timeout == 0;
        } catch (final SecurityException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot get Settings.Secure lock_screen_lock_after_timeout", e);
            return true;
        }
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.5 (L1) Ensure 'Power button instantly locks' is set to 'Enabled'
     *
     * @param context Application Context
     * @return
     */
    public static boolean isInstantPowerLockSet(final Context context) {
        try {
            final int result = Settings.Secure.getInt(context.getContentResolver(), "power_button_instantly_locks", -1);
            return result == 1;
        } catch (final SecurityException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot get Settings.Secure power_button_instantly_locks", e);
            return true;
        }
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.6 (L1) Ensure 'Lock Screen Message' is configured
     *
     * @param context Application Context
     * @return
     */
    public static boolean isLockScreenMessageSet(Context context) {
        return false;
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.7 (L2) Do not connect to untrusted Wi-Fi networks
     *
     * @param context Application Context
     * @return
     */
    public static boolean isWiFiSecure(Context context) {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wi = wifi.getConnectionInfo();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            int type = wi.getCurrentSecurityType();
            return type != WifiInfo.SECURITY_TYPE_OPEN;
        } else {
            // TODO: for SDK 30 how to check WiFi status?
            return true;
        }
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.8 (L2) Ensure 'Show passwords' is set to 'Disabled'
     *
     * @param context Application Context
     * @return
     */
    public static boolean isPasswordHidden(Context context) {
        int show = Settings.System.getInt(context.getContentResolver(), Settings.System.TEXT_SHOW_PASSWORD, 0);
        return show == 0;
    }

    /**
     * CIS_Google_Android_Benchmark_v1.4.0
     * 1.9 (L1) Ensure 'Developer Options' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if Developer Options are enabled
     */
    public static boolean isDeveloperModeEnabled(Context context) {
        int dev = Settings.Global.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0);
        return dev != 0;
    }

    /**
     * Continuation of CIS Android Benchmark 1.9
     *
     * @param context Application Context
     * @return true if ADB is enabled
     */
    public static boolean isAdbEnabled(Context context) {
        int adb = Settings.Global.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, 0);
        return adb != 0;
    }

    /**
     * 1.10 (L1) Ensure 'Install unknown apps' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if any app is allowed to install unknown apps
     */
    public static boolean isUnknownSourcesDisabled(Context context) {
        return false;
    }

    /**
     * 1.11 (L1) Do not root a user device
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isDeviceRooted(Context context) {
        return RootTool.isRooted(context.getPackageManager());
    }

    /**
     * 1.12 (L2) Ensure 'Smart Lock' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isSmartLockEnabled(Context context) {
        return false;
    }

    /**
     * 1.13 (L2) Ensure 'Lock SIM card' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isSimLockEnabled(Context context) {
        return false;
    }

    /**
     * 1.14 (L2) Ensure 'Find My Device' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isFindMyDeviceEnabled(Context context) {
        return false;
    }

    /**
     * 1.15 (L1) Ensure 'Use network-provided time' and 'Use networkprovided time zone' are set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isNetWorkProvidedTimeEnabled(Context context) {
        return false;
    }

    /**
     * 1.16 (L1) Ensure 'Remotely locate this device' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isLocationTrackingEnabled(Context context) {
        return false;
    }

    /**
     * 1.17 (L1) Ensure 'Allow remote lock and erase' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isRemoteWipeEnabled(Context context) {
        return false;
    }

    /**
     * 1.18 (L1) Ensure 'Scan device for security threats' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isAppScanningEnabled(Context context) {
        return false;
    }

    /**
     * 1.19 (L1) Ensure 'Improve harmful app detection' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isNewAppSubmissionEnabled(Context context) {
        return false;
    }

    /**
     * 1.20 (L1) Ensure 'Ask for unlock pattern/PIN/password before unpinning' is set to 'Enabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isUnpinningSecure(Context context) {
        return false;
    }

    /**
     * 1.21 (L1) Ensure 'Screen timeout' is set to '1 minute or less'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isScreenTimeoutShort(Context context) {
        return false;
    }

    /**
     * 1.22 (L1) Ensure 'Wi-Fi assistant' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isWifiAssistantDisabled(Context context) {
        return false;
    }

    /**
     * 1.23 (L1) Keep device Apps up to date
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isAutoAppUpdateEnabled(Context context) {
        return false;
    }

    /**
     * 1.24 (L1) Ensure 'Add users from lock screen' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isAddUsersDisabled(Context context) {
        return false;
    }

    /**
     * 1.25 (L1) Ensure 'Guest profiles' do not exist
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isGuestProfilesDisabled(Context context) {
        return false;
    }

    // 1.26 (L1) Review app permissions periodically

    /**
     * 1.27 (L1) Ensure 'Instant apps' is set to 'Disabled'
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isInstantAppsDisabled(Context context) {
        return false;
    }

    /**
     * 1.28 (L1) Ensure 'Bluetooth' Pairing is Configured
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isBluetoothEnabled(Context context) {
        int bt = Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.BLUETOOTH_ON, 0);
        return bt != 0 || isBluetoothAdapterEnabled(context);
    }

    /**
     * 1.29 (L1) Ensure that no 3rd party keyboards are installed
     *
     * @param context Application Context
     * @return true if device is rooted
     */
    public static boolean isKeyboardAppInstalled(Context context) {
        return false;
    }

    // 1.30 (L1) Review existing user profiles periodically

    public static boolean isLockAutomaticallySet(Context context) {
        return false;
    }

    public static boolean isBluetoothAdapterEnabled(Context context) {
        try {
            BluetoothManager blueMan = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            BluetoothAdapter btAdapter = blueMan.getAdapter();
            return btAdapter != null && btAdapter.isEnabled();
        } catch (Exception e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot fetch Bluetooth status", e);
        }
        return false;
    }

    public static boolean isHotspotEnabled(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        try {

            Method method = wifiManager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            Boolean booleanValue = (Boolean) method.invoke(wifiManager, (Object[]) null);
            if (Boolean.TRUE.equals(booleanValue)) {
                return true;
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot fetch Hotspot status", e);
            return isHotspotEnabled2(wifiManager);
        }
        return false;
    }

    public static boolean isHotspotEnabled2(WifiManager wifiManager) {
        try {
            Method method = wifiManager.getClass().getDeclaredMethod("getWifiApState");
            method.setAccessible(true);
            Integer actualState = (Integer) method.invoke(wifiManager, (Object[]) null);
            if (Integer.valueOf(13).equals(actualState)) {
                return true;
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
            Log.d(DroidHelper.class.getCanonicalName(), "Cannot fetch Hotspot status using reflection", e);
        }
        return false;
    }

    public static String getOsName(int sdk) {
        switch (sdk) {
            case 1:
            case 2:
            case 3:
                return "Android Cupcake";
            case 4:
                return "Android Donut";
            case 5:
            case 6:
            case 7:
                return "Android Eclair";
            case 8:
                return "AndroidFroyo";
            case 9:
            case 10:
                return "Android Gingerbread";
            case 11:
            case 12:
            case 13:
                return "Android Honeycomb";
            case 14:
            case 15:
                return "Android Ice Cream Sandwich";
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                return "Android KitKat";
            case 21:
            case 22:
                return "Android Lollipop";
            case 23:
                return "Android Marshmallow";
            case 24:
            case 25:
                return "Android Nougat";
            case 26:
            case 27:
                return "Android Oreo";
            case 28:
                return "Android Pie";
            case 29:
                return "Android 10";
            case 30:
                return "Android 11";
            case 31:
            case 32:
                return "Android 12";
            case 33:
                return "Android 13";
            case 34:
                return "Android 14";
            case 35:
                return "Android 15";
            case 36:    // Assumed Future Name
                return "Android 16";
            case 37:    // Assumed Future Name
                return "Android 17";
            default:
                return "Android <Unknown>";
        }
    }


}
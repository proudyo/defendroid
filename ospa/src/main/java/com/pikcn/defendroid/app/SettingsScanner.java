package com.pikcn.defendroid.app;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public final class SettingsScanner {
    private SettingsScanner() { throw new UnsupportedOperationException(); }
    public static List<SettingsScanResult> systemAnalysis(final Context context) {
        final List<SettingsScanResult> result = new ArrayList<>();

        if (!DroidHelper.isSecurityPatched()) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.1 //
            result.add(new SettingsScanResult("Device does not have latest security patch", "Last security patch is older than 2 months", SettingsScanResult.SEVERITY_CRITICAL));
        }

        if (!DroidHelper.isDeviceSecured(context.getApplicationContext())) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.2 //
            result.add(new SettingsScanResult("No Password set", "Password must be set to protect your device", SettingsScanResult.SEVERITY_CRITICAL));
        }

        if (DroidHelper.isPatternVisible(context.getApplicationContext())) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.3 //
            result.add(new SettingsScanResult("Lock pattern is visible", "Lock Pattern must be hidden", SettingsScanResult.SEVERITY_HIGH));
        }

        if (!DroidHelper.isLockScreenTimeoutZero(context.getApplicationContext())) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.4 //
            result.add(new SettingsScanResult("Lock Immediately is not set", "Screen should lock immediately", SettingsScanResult.SEVERITY_HIGH));
        }

        if (!DroidHelper.isInstantPowerLockSet(context.getApplicationContext())) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.5 //
            result.add(new SettingsScanResult("Instant power lock is not set", "Power button should instantly lock the device", SettingsScanResult.SEVERITY_HIGH));
        }

        /*if (!DroidHelper.isLockScreenMessageSet(context.getApplicationContext())) {
            // CIS_Google_Android_Benchmark_v1.4.0 - 1.6 //
            result.add(new SettingsScanResult("title", "Description", "High"));
        }

        if (!DroidHelper.isLockAutomaticallySet(context.getApplicationContext())) {
            result.add(new SettingsScanResult("Automatic Lock is not set", "Automatically Lock should be set to Immediately", "CRITICAL"));
        }*/


        if (RootTool.isRooted(context.getPackageManager())) {
            result.add(new SettingsScanResult("This device is rooted", "Rooted devices are more vulnerable to malware attacks and do not receive security updates", SettingsScanResult.SEVERITY_CRITICAL));
        }

        if (DroidHelper.isDeveloperModeEnabled(context.getApplicationContext())) {
            result.add(new SettingsScanResult("Developer Options are Enabled", "Developer options must be disabled unless you use this device for Android app development or testing", SettingsScanResult.SEVERITY_CRITICAL));
        }

        if (DroidHelper.isAdbEnabled(context.getApplicationContext())) {
            result.add(new SettingsScanResult("ADB is Enabled", "ADB must be disabled unless you use this device for Android app development or testing", SettingsScanResult.SEVERITY_CRITICAL));
        }

        if (!DroidHelper.isPasswordHidden(context.getApplicationContext())) {
            result.add(new SettingsScanResult("Password is visible by default", "Password must be hidden by default", SettingsScanResult.SEVERITY_MEDIUM));
        }

        return result;
    }
}

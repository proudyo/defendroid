package com.pikcn.defendroid.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pikcn.defendroid.R;
import com.pikcn.defendroid.databinding.ActivityMainBinding;

public class MainDestinationChangedListener implements NavController.OnDestinationChangedListener {
    private ActivityMainBinding mainBinding;

    public MainDestinationChangedListener(final ActivityMainBinding mainBinding) {
        this.mainBinding = mainBinding;
    }

    @Override
    public void onDestinationChanged(@NonNull NavController navController, @NonNull NavDestination navDestination, @Nullable Bundle bundle) {
        final int currentId = navDestination.getId();
        final BottomNavigationView bottomNavigationView = mainBinding.appBarMain.contentMain.bottomNavView;
        final FloatingActionButton floatingActionButton = mainBinding.appBarMain.fab;
        final Toolbar toolbar = mainBinding.appBarMain.toolbar;

        if (bottomNavigationView != null) {
            if (currentId == R.id.nav_apps_advisory || currentId == R.id.nav_settings_advisory) {
                bottomNavigationView.setVisibility(View.VISIBLE);
            } else {
                bottomNavigationView.setVisibility(View.GONE);
            }
        }

        if (floatingActionButton != null) {
            if (currentId == R.id.nav_apps_advisory || currentId == R.id.nav_settings_advisory) {
                floatingActionButton.setVisibility(View.VISIBLE);
            } else {
                floatingActionButton.setVisibility(View.GONE);
            }
        }

        if (currentId == R.id.nav_eula) {
            toolbar.setNavigationIcon(android.R.drawable.ic_media_ff);
        } else if (currentId == R.id.nav_scan) {
            toolbar.setNavigationIcon(android.R.drawable.ic_popup_sync);
        }
    }
}
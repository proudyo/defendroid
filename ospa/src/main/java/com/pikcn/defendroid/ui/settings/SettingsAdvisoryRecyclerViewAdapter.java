package com.pikcn.defendroid.ui.settings;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.SettingsScanResult;
import com.pikcn.defendroid.databinding.FragmentSettingsAdvisoryBinding;

import java.util.List;

public class SettingsAdvisoryRecyclerViewAdapter extends RecyclerView.Adapter<SettingsAdvisoryViewHolder> {
    private final List<SettingsScanResult> mValues;

    public SettingsAdvisoryRecyclerViewAdapter(List<SettingsScanResult> items) {
        mValues = items;
    }

    @NonNull
    @Override
    public SettingsAdvisoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @NonNull final FragmentSettingsAdvisoryBinding view = FragmentSettingsAdvisoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SettingsAdvisoryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final SettingsAdvisoryViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitle.setText(holder.mItem.getTitle());
        holder.mDescription.setText(holder.mItem.getDescription());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}

package com.pikcn.defendroid.ui.settings;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.SettingsScanResult;
import com.pikcn.defendroid.databinding.FragmentSettingsAdvisoryBinding;

public class SettingsAdvisoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public final TextView mTitle;
    public final TextView mDescription;
    public SettingsScanResult mItem;

    public SettingsAdvisoryViewHolder(FragmentSettingsAdvisoryBinding binding) {
        super(binding.getRoot());
        binding.getRoot().setOnClickListener(this);
        mTitle = binding.resultTitle;
        mDescription = binding.resultDesc;
    }


    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + " ~ " + mDescription.getText() + "'";
    }

    @Override
    public void onClick(View v) {
        try {
            mItem.onClick(v);
        } catch (Exception e) {
            Log.d(SettingsAdvisoryViewHolder.class.getCanonicalName(), "Unexpected error in View.OnClickListener", e);
        }
    }
}

package com.pikcn.defendroid.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.pikcn.defendroid.R;

public class PreferencesFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
    }

    @Override
    public void onStart() {
        super.onStart();

        final Context context = getContext();
        if (context != null) {
            try {
                final Preference versionPref = findPreference("application_version");
                if (versionPref != null) {
                    final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    versionPref.setTitle("Application Version: " + packageInfo.versionName + " (" + packageInfo.getLongVersionCode() + ")");
                    versionPref.setOnPreferenceClickListener(this);
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.d(PreferencesFragment.class.getCanonicalName(), "Cannot resolve self package name", e);
            }

            final Preference srcPref = findPreference("application_source_code");
            if (srcPref != null) {
                srcPref.setOnPreferenceClickListener(this);
            }

            final Preference aboutPref = findPreference("about_pikcn");
            if (aboutPref != null) {
                aboutPref.setOnPreferenceClickListener(this);
            }

            final Preference licPref = findPreference("open_source_licenses");
            if (licPref != null) {
                licPref.setOnPreferenceClickListener(this);
            }
        }
    }

    @Override
    public boolean onPreferenceClick(@NonNull Preference preference) {
        final Context context = preference.getContext();
        final Intent intent;
        if("application_version".equals(preference.getKey())) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pikcn.defendroid"));
        } else if("application_source_code".equals(preference.getKey())) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/pikcn/defendroid"));
        } else if("about_pikcn".equals(preference.getKey())) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pikcn.com/about/"));
        } else {
            intent = null;
            final Activity activity = getActivity();
            if(activity!=null && "open_source_licenses".equals(preference.getKey())) {
                final NavController navController = Navigation.findNavController(activity, R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.nav_licences);
                return true;
            }
        }

        if(intent!=null && intent.resolveActivity(context.getPackageManager()) != null) {
            ContextCompat.startActivity(context, intent, null);
            return true;
        }

        return false;
    }
}